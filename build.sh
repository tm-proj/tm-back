#!/bin/bash
#
# @file		build.sh
# @brief	Script to build backend app image
# @author	Dominik Kala
# @date		2021-3-26
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

docker build . -t tm-back
