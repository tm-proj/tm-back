# Stacja pogodowa
## Projekt oprogramowania serwera
Aplikacja napisana w node.js. 
## Instalacja
- Pobierz repozytorium
- Zainstaluj [Dockera](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10)
- Zainstaluj [docker-compose](https://docs.docker.com/compose/install/)
### Za pomocą skryptu 
Opcja testowana na wirtualizowanym systemie Debian 10.9<br>
Buduje, dodaje nowy serwis i inicjalizuje bazę danych.
```bash
./install.sh -r rootPassword -u dbUserName -p dbUserPassword -a
``` 
Gdzie:
- -r rootPassword - hasło dla użytkownika root bazy danych
- -u dbUserName - nazwa użytkownika bazy danych
- -p dbUserPassword - hasło użytkownika
- -a - instalacja dla ARM (opcjonalne).

Jeżeli inicjalizacja bazy danych się nie powiedzie, w pierwszej kolejności sprawdź czy kontener `tm-mysql-app` jest aktywny. 
```
docker container ls
```
Jeżeli jest aktywny, spróbuj ponowić inicjalizację po chwili za pomocą skryptu, procedura opisana poniżej.. 

### Ręczna instalacja etap po etapie
1. Skonfiguruj aplikację w plikach `docker-compose.yml` oraz `db-init/setup-db.sql`, nazwy użytkowników w tych plikach powinny się zgadzać!
2. Zbuduj obraz
```
docker build . -t tm-back
```
2. Uruchom aplikację
  - Z lini komend
  ```
  docker-compose up
  ```
  - Utwórz serwis za pomocą skryptu `service/init-service.sh`
```
cd service && ./init-service.sh && cd ..
```
3. Zainicjalizuj bazę danych<br>
   Uruchom skrypt `db-init/init-db.sh`, aplikacja musi być uruchomiona!
```
cd db-init && ./init-db.sh -u dbUserName -p dbUserPassword && cd ..
```
Gdzie:
- -u - nazwa uprzywilejowanego użytkownika bazy (root)
- -p - hasło użytkownika.