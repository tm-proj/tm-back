#
# @file	  Dockerfile
# @brief  Docker file
# @author Dominik Kala
# @date	  2021-3-26
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

FROM node:alpine3.12

WORKDIR /usr/src/app

COPY app/ .

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk update && apk --no-cache --virtual build-dependencies add \
    python3 \
    make \
    g++ \
    && ln -sf python3 /usr/bin/python \
    && npm install \
    && apk del build-dependencies

EXPOSE 443 
CMD [ "npm", "start" ]
