/*
 * @file		mgr-core.js
 * @brief		Managers core
 * @author	Dominik Kala
 * @date		2021-05-01
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const dbConnFactory = require('../database/db')

/**
 * Manager core
 */
class MgrCore {
  /**
   * Default constructor
   */
  constructor() {
    this.dbConn = null
  }

  /**
   * Initialize database connection
   */
     async initDbConn() {
      const dbConn = await dbConnFactory
      .createConnection()
      .catch(err => {
        console.log('UserMgrCore.init err', err)
        throw new Error('UserMgrCore.init err')
      })
      this.dbConn = dbConn
    }
  
    /**
     * Close database connection
     */
    closeDbConn() {
      if(this.dbConn) this.dbConn.end()
    }
}

module.exports = {
  MgrCore
}