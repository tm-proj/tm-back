/*
 * @file		device-mgr.js
 * @brief		Device manager
 * @author	Dominik Kala
 * @date		2021-05-01
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const deviceTokenFactory = require('../objects/device-token-factory')
const mobileQueries = require('../query/mobile-queries')
const deviceQueries = require('../query/device-queries')
const deviceConfStatus = require('../objects/device-conf-status')
const { MgrCore } = require('./mgr-core')
const { User } = require("../objects/user")

/**
 * Device initialization manager core
 */
class DeviceMgrCore extends MgrCore {
  /**
   * Default constructor
   */
  constructor() {
    super()
  }

  /**
   * Check if the user has already a device with the specified name
   * @param {string} devName - Device name
   * @param {number} userId - User id 
   * @returns {Promise<Boolean>} A promise to the flag, true if the user has already a device with the same name, false otherwise.
   */
  checkIfDevNameUsed(devName, userId) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.checkIfDevNameUsed, [devName, userId, deviceConfStatus.REMOVED], (err, result) => {
        if(err) reject(err)
        else if (result.length != 1) reject(`DeviceInitMgrCore.checkIfDevNameUsed query returned ${result.length} rows, expexted only one`)
        else resolve(result[0].devNameUsed == true)
      })
    })
  }

  /**
   * Create new device in database
   * @param {string} devName - Device name 
   * @param {number} userId - User's id 
   * @returns {Promise<number>} A promise to a new device id
   */
  createNewDevice(devName, userId) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.preInitDevice, [devName, userId, deviceConfStatus.PREINITIALIZED], (err, result) => {
        if(err) reject(err)
        else resolve(result.insertId)
      })
    })
  }

  /**
   * Update device token
   * @param {number} deviceId - Device id 
   * @param {string} token - Device token 
   * @returns {Promise<DeviceToken>} - A promise to device token object
   */
  updateDeviceToken(deviceId, token) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.updateDeviceToken, [token.getToken(), deviceId], (err, result) => {
        if(err) reject(err)
        else resolve(token)
      })
    })
  }

  /**
   * Initialize device 
   * @param {DeviceToken} deviceToken - Device token 
   * @param {string} mac - Device MAC address 
   * @returns {Promise<boolean>} A promise to initialization status, true if found device and initialized successfully, false otherwise.
   */
  initDevice(deviceToken, mac) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(deviceQueries.initDevice, [mac, deviceConfStatus.INITIALIZED, deviceToken.getToken(), deviceConfStatus.PREINITIALIZED], (err, result) => {
        if(err) reject(err)
        else if (result.changedRows != 1) resolve(false)
        else resolve(true)
      })
    })
  }

  /**
   * Remove user's device
   * @param {number} userId - An id of user who wants to remove his device
   * @param {number} deviceId - Device id  
   * @returns {Promise<boolean>} A promise to operation status, true if found device and removed successfully, false otherwise. 
   */
  removeDevice(userId, deviceId) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.removeDevice, [deviceConfStatus.REMOVED, deviceId, deviceConfStatus.INITIALIZED, userId], (err, result) => {
        if(err) reject(err)
        else if (result.changedRows != 1) resolve(false)
        else resolve(true)
      })
    })
  }

  getUserDevicesList(userId) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.getUserDevices, [userId, deviceConfStatus.INITIALIZED], (err, result) => {
        if(err) reject(err)
        else if (result.length == 0) resolve([])
        else resolve(result)
      })
    })
  }
}

/**
 * Device initialization manager class
 */
class DeviceMgr {
  /**
   * Preinitialize device
   * @param {User} user - User object 
   * @param {string} devName - Device name
   * @returns {Promise<string | false>} A promise to newly added device token, or false if failed 
   */
  preInitDevice(user, devName) {
    const mgrCore = new DeviceMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.checkIfDevNameUsed(devName, user.getUserId()))
      .then(isNameUsed => {
        if(isNameUsed) resolve(false)
        else return mgrCore.createNewDevice(devName, user.getUserId())
      })
      .then(deviceId => {
        if(deviceId === undefined) resolve(false)
        else return mgrCore.updateDeviceToken(deviceId, deviceTokenFactory.fromDeviceId(deviceId))
      })
      .then(deviceToken => {
        if(deviceToken === undefined || !deviceToken) resolve(false)
        else resolve(deviceToken.getToken())
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * Initialize device 
   * @param {DeviceToken} deviceToken - Device token 
   * @param {string} mac - Device MAC address 
   * @returns {Promise<boolean>} A promise to initialization status, true if found device and initialized successfully, false otherwise.
   */
  initDevice(deviceToken, mac) {
    const mgrCore = new DeviceMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.initDevice(deviceToken, mac))
      .then(success => {
        if(success !== undefined) resolve(success)
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * Remove user's device
   * @param {User} user - A user who wants to remove his device
   * @param {number} deviceId - Device id  
   * @returns {Promise<boolean>} A promise to operation status, true if found device and removed successfully, false otherwise. 
   */
  removeDevice(user, deviceId) {
    const mgrCore = new DeviceMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.removeDevice(user.getUserId(), deviceId))
      .then(success => {
        if(success !== undefined) resolve(success)
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * Get list of user devices
   * @param {User} user - A user whose devices list shall be returned
   * @returns {Promise<>}
   */
  getUserDevicesList(user) {
    const mgrCore = new DeviceMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.getUserDevicesList(user.getUserId()))
      .then(devicesList => {
        if(devicesList !== undefined) resolve(devicesList)
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }
}

const deviceMgr = new DeviceMgr()
module.exports = deviceMgr
