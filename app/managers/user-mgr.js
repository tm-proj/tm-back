/*
 * @file		user-mgr.js
 * @brief		Backend user manager
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const jwt = require('jsonwebtoken')
const bcrypt = require ('bcrypt')

const { MgrCore } = require('./mgr-core')
const mobileQueries = require('../query/mobile-queries')
const credentialsMgr = require('../conf/credentials-mgr')
const { User } = require("../objects/user")


/**
 * User manager core
 */
class UserMgrCore extends MgrCore {
  /**
   * Default constructor
   */
  constructor() {
    super()
  }

  /**
   * Check if provided login has not been used before by any other user
   * @param {string} login - Login
   * @returns {Promise<boolean>} A promise to boolean flag
   */
  checkIfLoginUsed(login) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.checkIfLoginNotUsed, [login], (err, result) => {
        if(err) reject(err)
        else if(result.length != 1) reject(`UserMgrCore.checkIfLoginNotUsed query returned ${result.length} rows, expexted only one`)
        else resolve(result[0].loginUsed == true)
      })
    })
  }

  /**
   * Create new user
   * @param {string} login - User's login
   * @param {string} password - User's password
   * @returns {Promise<User>} A promise to new user's object
   */
  createNewUser(login, password) {
    return new Promise(async (resolve, reject) => {
      const rounds = 10
      const hash = await bcrypt.hash(password, rounds)
      this.dbConn.query(mobileQueries.createNewUser, [login, hash], (err, result) => {
        if(err) reject(err)
        else resolve(new User(result.insertId, login))
      })
    })
  }

  /**
   * Sign in user
   * @param {string} login - User's login
   * @param {string} password  - User's password
   * @returns {Promise<User | false>} A promise to user's object. If the login process fails promise returns false instead of user's object.
   */
  signIn(login, password) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.signInUser, [login], async (err, result) => {
        if(err) reject(err)
        else if(result.length != 1) resolve(false)
        else {
          const isPassValid = await bcrypt.compare(password, result[0].password)
          if(isPassValid) resolve(new User(result[0].userId, login))
          else resolve(false)
        }
      })
    })
  }

  /**
   * Get user by ID
   * @param {number} userId - User's id
   * @returns {Promise<User | false>} A promise to user's object. If this process fails promise returns false instead of user's object.
   */
  getUserById(userId) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.userById, [userId], (err, result) => {
        if(err) reject(err)
        else if(result.length != 1) resolve(false)
        else resolve(new User(result[0].userId, result[0].login))
      })
    })
  }

  /**
   * Get secret token for JWT
   * @returns {string} Sectret
   */
  getTokenSecret() {
    return credentialsMgr.hasCredentials() ? credentialsMgr.getCredentials().key : 'top-secret'
  }

  /**
   * Convert user id to JWT
   * @param {number} userId - User id
   * @returns {string} Created JWT
   */
  userIdToToken(userId) {
    const secret = this.getTokenSecret()
    return jwt.sign({userId: userId}, secret, {
      expiresIn: '24h'
    })
  }

  /**
   * Convert JWT to user id
   * @param {string} token - JWT
   * @returns {Promise<number | false} A promise to user's id. If JWT is invalid or expired promise returns false.
   */
  tokenToUserId(token) {
    return new Promise((resolve, reject) => {
      const secret = this.getTokenSecret()
      jwt.verify(token, secret, (err, decoded) => {
        if(err) resolve(false)
        else if (decoded.userId === false) resolve(false)
        else resolve(decoded.userId)
      })
    })
  }
}


/**
 * Backend user manager
 */
class UserMgr {
  /**
   * Sign up user
   * @throws Errors when server error occures
   * @param {string} login - user login
   * @param {string} password - user's password
   * @returns {Promise<string | false>} A promise to new user's token. If a login has been already used promise returns false instead of the token string.
   */
  signUp(login, password) {
    const mgrCore = new UserMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.checkIfLoginUsed(login))
      .then(isUsed => {
        if(isUsed) resolve(false)
        else return mgrCore.createNewUser(login, password)
      })
      .then(user => {
        if (user === undefined || user === false) resolve(false)
        else resolve(mgrCore.userIdToToken(user.getUserId()))
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * Sign in user
   * @throws Errors when server error occures
   * @param {string} login - user login
   * @param {string} password - user's password
   * @returns {Promise<string | false>} A promise to user's token. If provided authentication data is invalid it returns false instead of the token string.
   */
  signIn(login, password) {
    const mgrCore = new UserMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.signIn(login, password))
      .then(user => {
        if(user === undefined || user === false) resolve(false)
        else resolve(mgrCore.userIdToToken(user.getUserId()))
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * Get user by token
   * @throws Errors when server error occures
   * @param {string} token - user login
   * @returns {Promise<User | false>} A promise to user's object. If provided token is invalid it returns false instead of the user object.
   */
  getByToken(token) {
    const mgrCore = new UserMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.tokenToUserId(token))
      .then(userId => {
        if(userId === undefined || userId === false) resolve(false)
        else return mgrCore.getUserById(userId)
      })
      .then(user => resolve(user))
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }
}

const userMgr = new UserMgr()
module.exports = userMgr
