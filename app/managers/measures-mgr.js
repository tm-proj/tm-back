/*
 * @file		measures-mgr.js
 * @brief		Measures manager
 * @author	Dominik Kala
 * @date		2021-05-04
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const { MgrCore } = require("./mgr-core")
const { User } = require("../objects/user")
const deviceQueries = require('../query/device-queries')
const mobileQueries = require('../query/mobile-queries')
const deviceTokenFactory = require('../objects/device-token-factory')
const deviceConfStatus = require('../objects/device-conf-status')
const measureFactory = require('../objects/measure')


/**
 * Measures manager core
 */
class MeasuresMgrCore extends MgrCore {
  /**
   * Default constructor
   */
  constructor() {
    super()
  }

  /**
   * 
   * @param {DeviceToken} deviceToken 
   * @returns 
   */
  getDeviceId(deviceToken) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(deviceQueries.getDeviceId, [deviceToken.getToken(), deviceConfStatus.INITIALIZED], (err, result) => {
          if(err) reject(err)
          else if(result.length != 1) resolve(false)
          else resolve(result[0].deviceId)
      })
    })
  }

  /**
   * 
   * @param {number} deviceId 
   * @param {Measure} measure 
   * @returns 
   */
  saveMeasure(deviceId, measure) {
    return new Promise((resolve, reject) => {
      this.dbConn.query(deviceQueries.saveMeasure, 
        [measure.getDbUtcTimestamp(), measure.getTemperature(), measure.getHumidity(), measure.getPm2_5(), measure.getPm10(), deviceId], 
        (err, result) => {
          if(err) reject(err)
          else resolve(result.affectedRows == 1)
      })
    })
  }

  getUserDevicesQueue(userId, deviceIds) {
    const selectAll = deviceIds === false ? 1 : 0
    if(deviceIds === false) deviceIds = [0]
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.getUserDevicesQueue, [userId, deviceConfStatus.INITIALIZED, deviceIds, selectAll], (err, result) => {
        if(err) reject(err)
        if(result.length == 0) resolve(false)
        else resolve(result)
      })
    })
  }

  getMeasures(deviceId, deviceName, latestNum) {
    const rowsLimit = latestNum === false ? 1 : latestNum
    return new Promise((resolve, reject) => {
      this.dbConn.query(mobileQueries.getMeasures, [deviceId, rowsLimit], (err, result) => {
        if(err) reject(err)
        else {
          let deviceMeasures = {
            deviceId: deviceId,
            name: deviceName,
            measures: []
          }
          result.forEach(row => {
            const dbUtcTimestamp = row.utcTimestamp.toISOString().slice(0, 19).replace('T', ' ')
            deviceMeasures.measures.push(measureFactory.fromDbUtcTimestamp(dbUtcTimestamp, row.temp, row.humidity, row.pm2_5, row.pm10))
          })
          resolve(deviceMeasures)
        }
      })
    })
  }
}

/**
 * Measures manager
 */
class MeasuresMgr {
  /**
   * 
   * @param {User} user 
   * @param {number[]} deviceIds 
   * @param {number} latestNum 
   */
  getMeasures(user, deviceIds, latestNum) {
    const mgrCore = new MeasuresMgrCore()
    let data = {devices: []}
    return new Promise(async (resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.getUserDevicesQueue(user.getUserId(), deviceIds))
      .then(async devicesQueue => {
        if(devicesQueue === undefined) resolve([])
        else {
          while(devicesQueue.length > 0) {
            const devInfo = devicesQueue.pop()
            const measures = await mgrCore.getMeasures(devInfo.deviceId, devInfo.name, latestNum)
            data.devices.push(measures)
          }
          resolve(data)
        }
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }

  /**
   * 
   * @param {*} deviceToken 
   * @param {*} measure 
   * @returns 
   */
  saveMeasure(deviceToken, measure) {
    const mgrCore = new MeasuresMgrCore()
    return new Promise((resolve, reject) => {
      mgrCore.initDbConn()
      .then(() => mgrCore.getDeviceId(deviceToken))
      .then(deviceId => {
        if(deviceId === undefined || !deviceId) resolve(false)
        else return mgrCore.saveMeasure(deviceId, measure)
      })
      .then(success => {
        if(success !== undefined) resolve(success)
      })
      .then(() => mgrCore.closeDbConn())
      .catch(err => reject(err))
    })
  }
}

const measuresMgr = new MeasuresMgr()
module.exports = measuresMgr
