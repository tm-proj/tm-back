/*
 * @file		logger-mgr.js
 * @brief		Application logger manager
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const morgan = require('morgan')
const rfs = require('rotating-file-stream')
const path = require('path')

const confMgr = require('./conf-mgr')

/**
 * Application logger manager
 */
class LoggerMgr {

  /**
   * Set up app logger
   * @param {Express} app - application to set up logger for
   */
  setUpLogger(app) {
    const logsConf = confMgr.getLogsConf()
    const logsDirPath = path.join(global.appRoot || __dirname, logsConf.directory)

    // Set up dev logger 
    if(logsConf.debugMode) {
      app.use(morgan('dev', {
        skip: (req, res) => (res.statusCode < 400)
      }))
    }

    // Create a rotating write stream
    var accessLogStream = rfs.createStream('access.log', {
      interval: '1d', // rotate daily
      path: logsDirPath
    })

    // Set up common logger
    app.use(morgan('combined', { stream: accessLogStream }))
  }
}

const loggerMgr = new LoggerMgr()
module.exports.setUpLogger = app => loggerMgr.setUpLogger(app)
