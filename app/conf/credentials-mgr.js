/*
 * @file		credentials-mgr.js
 * @brief		Credentials manager
 * @author	Dominik Kala
 * @date		2021-4-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const confMgr = require('./conf-mgr')
const fs = require("fs")

/**
 * @typedef {Object} Credentials
 * @property {string} key - private key
 * @property {string} cert - certificate
 */

/**
 * Api credentials manager
 */
class CredentialsMgr {
  /**
   * Check if credentials are available
   * @returns {boolean} 
   * @retval true - if has credentials
   * @retval false - if has not credentials
   */
  hasCredentials() {
    const paths = confMgr.getCredentialsPaths()
    return fs.existsSync(paths.keyPath) && fs.existsSync(paths.certPath)
  }

  /**
   * 
   * @returns {Credentials} Credentials object
   */
  getCredentials() {
    // TODO: Catch exceptions, and stop launching app? 
    const paths = confMgr.getCredentialsPaths()
    return {
      key: fs.readFileSync(paths.keyPath),
      cert: fs.readFileSync(paths.certPath),
      ca: fs.readFileSync(paths.caPath),
      requestCert: true,
      rejectUnauthorized: false
    }
  }
}

const credentialsMgr = new CredentialsMgr()
module.exports = credentialsMgr
