/*
 * @file		conf.js
 * @brief		Configuration manager
 * @author	Dominik Kala
 * @date		2021-04-05
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const fs = require('fs')
const ini = require('ini')
const path = require('path')

const INI_FILE_NAME = 'app-conf.ini'

/**
 * @typedef {Object} DbConf
 * @property {string} host - DB host address
 * @property {number} port - DB port
 * @property {string} user - DB user
 * @property {string} password - DB uset password
 * @property {string} database - Database name
 */

/**
 * @typedef {Object} CredentialsPaths
 * @property {string} keyPath - path to the private key
 * @property {string} certPath - path to the certificate
 */

/**
 * @typedef {Object} LogsConf
 * @property {string} directory - directory name
 * @property {boolean} debugMode - debug mode flag
 */

/**
 * Api configuration manager
 */
class ConfMgr {

  /**
   * Load app configuration
   * @note It should be called at the app start
   */
  load() {
    const iniDirPath = path.join(global.appRoot || __dirname, INI_FILE_NAME)
    let iniConf = {
      app: {},
      logs: {},
      db: {},
      credentials: {}
    }

    try {
      iniConf = ini.parse(fs.readFileSync(iniDirPath, 'utf-8'))
    }
    catch (err) {
      console.log('App ini file reading error', err.message)
    }

    this.appPort = process.env.PORT || iniConf.app.port || 80
    
    this.logsConf = {
      directory: iniConf.logs.directory       || 'logs',
      debugMode: (iniConf.logs.debug === '1') || false
    }

    this.dbConf = {
      host:     process.env.MYSQL_HOST      || iniConf.db.host     || 'localhost',
      port:     process.env.MYSQL_PORT      || iniConf.db.port     || 3306,
      user:     process.env.MYSQL_USER      || iniConf.db.user     || 'user',
      password: process.env.MYSQL_USER_PASS || iniConf.db.password || 'password',
      database: process.env.MYSQL_DB        || iniConf.db.database || 'db',
    }

    this.credentialsPaths = {
      keyPath: iniConf.credentials.keyPath    || '',
      certPath: iniConf.credentials.certPath  || '',
      caPath: iniConf.credentials.caPath      || ''
    }
  }

  /**
   * Get app port
   * @returns {number} App port number
   */
  getAppPort() {
    return this.appPort
  }

  /**
   * Get logs configuration object
   * @returns {LogsConf} Logs configuration object
   */
  getLogsConf() {
    return this.logsConf
  }

  /**
   * Get DB connection configuration
   * @returns {DbConf} DB connection configuration object
   */
  getDbConf() {
    return this.dbConf
  }

  /**
   * Get credentials paths 
   * @returns {CredentialsPaths} Credentials paths object
   */
  getCredentialsPaths() {
    return this.credentialsPaths
  }
}

const confMgr = new ConfMgr()
module.exports = confMgr
