/*
 * @file		db.js
 * @brief		Database connection factory
 * @author	Dominik Kala
 * @date		2021-04-05
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const mysql = require('mysql')
const conf = require('../conf/conf-mgr')

/**
 * DB connection factory
 */
class DataBaseConnFactory {
  /**
   * Create DB connection
   * 
   * Simple example
   * ```js
   * // Usage snippet
   * createConnection()
   *  .then(conn => {
   *    // Got DB connection
   *    conn.end()
   *  })
   *  .catch(reason => {
   *    // Connection failed
   *  })
   * ```
   * @return {Promise<mysql.MysqlError>} Connection error 
   */
  createConnection() {
    let conn = mysql.createConnection(conf.getDbConf())
    return new Promise((resolve, reject) => 
      conn.connect(err => {
        if(err) {
          console.log("DataBaseConnFactory error:", err.message)
          reject(err)
        }
        else {
          resolve(conn)
        }
      })  
    )
  }  
}

const dbConnFactory = new DataBaseConnFactory()
module.exports = dbConnFactory
