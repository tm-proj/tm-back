/*
 * @file		user.js
 * @brief		Backend api user
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

/**
 * Backend user class
 */
class User {
  /**
   * Constructor
   * @param {number} userId - user id
   * @param {string} login - user login
   */
  constructor(userId, login) {
    this.userId = userId
    this.login = login
  }

  /**
   * Getter
   * @returns {number} user id
   */
  getUserId() {
    return this.userId
  }

  /**
   * Getter
   * @returns {string} user login
   */
  getLogin() {
    return this.login
  }
}

module.exports = {
  User
} 
