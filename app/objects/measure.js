/*
 * @file		measure.js
 * @brief		Measure data type
 * @author	Dominik Kala
 * @date		2021-05-04
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

class Measure {
  /**
   * Default constructor
   * @param {number} utcUnixTimestamp 
   * @param {number} temperature 
   * @param {number} humidity 
   * @param {number} pm2_5 
   * @param {number} pm10 
   */
  constructor(utcUnixTimestamp, temperature, humidity, pm2_5, pm10) {
    this.utcUnixTimestamp = utcUnixTimestamp
    this.temperature = temperature
    this.humidity = humidity
    this.pm2_5 = pm2_5
    this.pm10 = pm10
  }

  getUtcUnixTimestamp() {
    return this.utcUnixTimestamp
  }

  getDbUtcTimestamp() {
    let timestamp = new Date()
    timestamp.setTime(this.utcUnixTimestamp * 1000)
    return timestamp.toISOString().slice(0, 19).replace('T', ' ')
  }

  getTemperature() {
    return this.temperature
  }

  getHumidity() { 
    return this.humidity
  }

  getPm2_5() {
    return this.pm2_5
  }

  getPm10() {
    return this.pm10
  }
}

class MeasureFactory {
  fromRawData(temperature, humidity, pm2_5, pm10) {
    const utcUnixTimestamp = Math.round((new Date()).getTime() / 1000)
    return new Measure(utcUnixTimestamp, temperature, humidity, pm2_5, pm10)
  }

  fromUtcUnix(utcUnixTimestamp, temperature, humidity, pm2_5, pm10) {
    return new Measure(utcUnixTimestamp, temperature, humidity, pm2_5, pm10)
  }

  /**
   * 
   * @param {string} dbTimestamp - DB timestamp in format YYYY-MM-DD HH:MI:SS
   * @param {*} temperature 
   * @param {*} humidity 
   * @param {*} pm2_5 
   * @param {*} pm10 
   * @returns 
   */
  fromDbUtcTimestamp(dbTimestamp, temperature, humidity, pm2_5, pm10) {
    const isoTimestamp = dbTimestamp.replace(' ', 'T') + '.000Z'
    const utcUnixTimestamp = Math.round((new Date(isoTimestamp)).getTime() / 1000)
    return new Measure(utcUnixTimestamp, temperature, humidity, pm2_5, pm10)
  }
}

const measureFactory = new MeasureFactory()
module.exports = measureFactory
