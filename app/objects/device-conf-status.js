/*
 * @file		device-conf-status.js
 * @brief		Device configuration status
 * @author	Dominik Kala
 * @date		2021-05-01
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

module.exports = {
  PREINITIALIZED: 1,
  INITIALIZED: 2,
  REMOVED: 3
}
