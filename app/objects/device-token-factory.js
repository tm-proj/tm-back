/*
 * @file		device-token-factory.js
 * @brief		Device token factory
 * @author	Dominik Kala
 * @date		2021-05-01
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use-strict'

const crc = require('node-crc')

/**
 * Device token class
 */
class DeviceToken {
  /**
   * Constructor
   * @param {string} token - Token in hex string buffer
   */
  constructor(token) {
    this.token = token
  }

  /**
   * Get device token
   * @returns {string} Device token hex string
   */
  getToken() {
    return this.token
  }

  /**
   * Check if create token is valid
   * @returns {boolean} True is token is valid, false otherwise
   */
  isValid() {
    if(this.token === undefined) return false

    // Create token buffer
    let tokenBuf = Buffer.alloc(8)
    tokenBuf.write(this.token, 0, 8, 'hex') 

    // Slice token to device id and crc
    const devIdBuf = tokenBuf.slice(0, 4)
    const devIdCrcBuf = tokenBuf.slice(4, 8)

    // Compute crc of device id
    const devIdCrcClcBuf = crc.crc32(devIdBuf)
    if(!devIdCrcBuf) return false

    // Compare crcs
    const res = devIdCrcBuf.compare(devIdCrcClcBuf, 0, 4, 0, 4)

    return (res == 0)
  }
}

/**
 * Device token factory class
 */
class DeviceTokenFactory {
  /**
   * Create device token from existing token
   * @param {BigInt} token - Device token
   * @returns {DeviceToken} Device token instance
   */
  fromDeviceToken(token) {
    return new DeviceToken(token)
  }

  /**
   * Create device token from device id
   * @param {number} deviceId - Device id
   * @returns {DeviceToken} Device token instance
   */
  fromDeviceId(deviceId) {
    // Device id to buf
    let devIdBuf = Buffer.alloc(4)
    devIdBuf.writeUInt32LE(deviceId)
    
    // Compute check sum
    const devIdCrcBuf = crc.crc32(devIdBuf)
    if(!devIdCrcBuf) return false

    // Create token
    let tokenBuf = Buffer.alloc(8)
    devIdBuf.copy(tokenBuf, 0, 0)
    devIdCrcBuf.copy(tokenBuf, 4, 0)

    // Convert token to bigInt
    const token = tokenBuf.toString('hex')

    return new DeviceToken(token)
  }
}

const deviceTokenFactory = new DeviceTokenFactory()
module.exports = deviceTokenFactory
