/*
 * @file		route.js
 * @brief		Defines paths to all of the APi endpoints
 * @author	Dominik Kala
 * @date		2021-04-05
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const express = require('express')
const mobileControler = require('../controlers/mobile-controler')
const deviceControler = require('../controlers/device-controler')

const router = express.Router()

// Routes for the mobile app 
router.post('/mobile/auth/signup', mobileControler.signUp)
router.post('/mobile/auth/signin', mobileControler.signIn)
router.post('/mobile/get-measures', mobileControler.getMeasures)
router.get('/mobile/get-devices', mobileControler.getDevices)
router.post('/mobile/preinit-device', mobileControler.preinitDevice)
router.delete('/mobile/delete-device', mobileControler.deleteDevice)

// Routes for the device
router.post('/device/init', deviceControler.initDevice)
router.post('/device/save-measure', deviceControler.saveMeasure)

module.exports = router
