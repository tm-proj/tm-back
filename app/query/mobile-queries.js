/*
 * @file		mobile-queries.js
 * @brief		Queries for mobile app endpoint
 * @author	Dominik Kala
 * @date		2021-4-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

module.exports = {
  checkIfLoginNotUsed: "SELECT (CASE WHEN COUNT(*) = 0 THEN FALSE ELSE TRUE END) AS `loginUsed` FROM `tm_user` tmu WHERE tmu.login LIKE ?;",
  createNewUser: "INSERT INTO `tm_user` (`login`, `password`) VALUES (?, ?);",
  signInUser: "SELECT tmu.id_user AS userId, tmu.password FROM `tm_user` tmu WHERE tmu.login LIKE ?;",
  userById: "SELECT tmu.id_user AS userId, tmu.login FROM `tm_user` tmu WHERE tmu.id_user=?;",
  checkIfDevNameUsed: "SELECT (CASE WHEN COUNT(*) = 0 THEN FALSE ELSE TRUE END) AS `devNameUsed` FROM `tm_device` tmd WHERE tmd.name LIKE ? AND tmd.id_user=? AND tmd.id_dev_conf_status<>?;",
  preInitDevice: "INSERT INTO `tm_device` (`name`, `id_user`, `id_dev_conf_status`) VALUES (?, ?, ?);",
  updateDeviceToken: "UPDATE `tm_device` SET `token`=? WHERE `id_device`=?;",
  removeDevice: "UPDATE `tm_device` SET `id_dev_conf_status`=? WHERE `id_device`=? AND `id_dev_conf_status`=? AND `id_user`=?;",
  getUserDevices: "SELECT tmd.id_device AS 'deviceId', tmd.name FROM `tm_device` tmd WHERE tmd.id_user=? AND tmd.id_dev_conf_status=?;",
  getUserDevicesQueue: "SELECT tmd.id_device AS 'deviceId', tmd.name FROM `tm_device` tmd WHERE tmd.id_user=? AND tmd.id_dev_conf_status=? AND (tmd.id_device IN (?) OR 1=?);",
  getMeasures: "SELECT tmm.measure_utc_timestamp AS 'utcTimestamp', tmm.temp, tmm.humidity, tmm.pm2_5, tmm.pm10 FROM `tm_measure` tmm WHERE tmm.id_device=? ORDER BY tmm.id_measure DESC LIMIT ?;"
}
