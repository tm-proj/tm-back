/*
 * @file		device-queries.js
 * @brief		Queries for device endpoints
 * @author	Dominik Kala
 * @date		2021-05-03
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

module.exports = {
  initDevice: "UPDATE `tm_device` SET `mac`=?, `id_dev_conf_status`=? WHERE `token` LIKE ? AND `id_dev_conf_status`=?;",
  getDeviceId: "SELECT tmd.id_device AS 'deviceId' FROM `tm_device` AS tmd WHERE tmd.token LIKE ? AND tmd.id_dev_conf_status=?",
  saveMeasure: "INSERT INTO `tm_measure` (`measure_utc_timestamp`, `temp`, `humidity`, `pm2_5`, `pm10`, `id_device`) VALUES (?, ?, ?, ?, ?, ?)"
}
