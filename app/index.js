/*
 * @file		index.js
 * @brief	Backend application main file 
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const express = require('express')
const https = require('https')
const path = require('path')

const confMgr = require('./conf/conf-mgr')
const credentialsMgr = require('./conf/credentials-mgr')
const loggerMgr = require('./conf/logger-mgr')
const router = require('./routes/route')


// Set app root path 
global.appRoot = path.resolve(__dirname)

// Load app configuration 
confMgr.load()

// Create app
const app = express()

// Get port to listen on 
const appPort = confMgr.getAppPort()


// Parse application/x-www-form-urlencoded
app.use(express.urlencoded())

// This application does not expect large data structures,
// so limit set to 100kb should be enough
app.use(express.json({ limit: '100kb' }))

// Set up logger
loggerMgr.setUpLogger(app)

// Set API router
app.use(router)


/**
 * Callback called when server has been started
 * @param {Object} err - Error description 
 */
const onServerStarted = err => {
   if(err) {
      console.log('Cannot start the server', err)
   }
   else {
      console.log(`Server started, listening on ${appPort}`)
   }
}

// Start server
if(credentialsMgr.hasCredentials()) {
   // Run application in https mode
   console.log('Credentials are available, starting server in https mode')
   const credentials = credentialsMgr.getCredentials()
   const httpsSrv = https.createServer(credentials, app)
   httpsSrv.listen(appPort, onServerStarted)
}
else {
   // Run application in http mode
   console.log('No credentials, starting server in http mode')
   app.listen(appPort, onServerStarted)
}
