/*
 * @file		mobile-controler.js
 * @brief		Controler for the mobile app routes 
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const inputValidator = require('../validators/input-validator')
const userMgr = require('../managers/user-mgr')
const devMgr = require('../managers/device-mgr')
const measureMgr = require('../managers/measures-mgr')
const controlerResponses = require('./controler-responeses')

/**
 * Helper for mobile controler
 */
class MobileControlerHelper {
  /**
   * 
   * @param {*} headers - request headers
   * @param {*} res - connection response 
   * @returns {User | false} User's object or false if operation fails
   */
  async getUserByTokenOrSendStatus(headers, res) {
    try {
      const token = inputValidator.getValidParam(headers['x-access-token'], inputValidator.isValidJwt)
      if(!token) {
        controlerResponses.sendMissingHeaderError(res)
      }
      else {
        const user = await userMgr.getByToken(token)
        if(!user) controlerResponses.sendInvalidTokenError(res)
        else return user
      }
    }
    catch (err) {
      console.log(err)
      controlerResponses.sendInternalServerError(res)
    }
    return false
  }
}

/**
 * Controler for the mobile app routes 
 */
class MobileControler {
  async signUp(req, res) {
    const login = inputValidator.getValidParam(req.body.login, inputValidator.isValidLogin)
    const pass = inputValidator.getValidParam(req.body.pass, inputValidator.isValidPassword)

    if(!login || !pass) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      await userMgr.signUp(login, pass)
      .then(token => {
        if(token === false) controlerResponses.sendLoginAlreadyUsedError(res)
        else controlerResponses.sendStatusCreated(res, {token: token})
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }    
  }

  async signIn(req, res) {
    const login = inputValidator.getValidParam(req.body.login, inputValidator.isValidLogin)
    const pass = inputValidator.getValidParam(req.body.pass, inputValidator.isValidPassword)

    if(!login || !pass) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      await userMgr.signIn(login, pass)
      .then(token => {
        if(token === false) controlerResponses.sendWronAuthenticationDataError(res)
        else controlerResponses.sendStatusOk(res, {token: token})
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }

  async getMeasures(req, res) {
    const helper = new MobileControlerHelper() 
    const user = await helper.getUserByTokenOrSendStatus(req.headers, res)
    const deviceIds = inputValidator.getValidArray(req.body.deviceIds, inputValidator.isValidIntNumber)
    const latestNum = inputValidator.getValidParam(req.body.latestNum, inputValidator.isValidIntNumber)

    if(!user) {
      // Do nothing
    }
    else {
      measureMgr.getMeasures(user, deviceIds, latestNum)
      .then(data => controlerResponses.sendStatusOk(res, data))
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }

  async getDevices(req, res) {
    const helper = new MobileControlerHelper() 
    const user = await helper.getUserByTokenOrSendStatus(req.headers, res)
    
    if(!user) {
      // Do nothing
    }
    else {
      await devMgr.getUserDevicesList(user)
      .then(devicesList => controlerResponses.sendStatusOk(res, {devices: devicesList}))
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }

  async preinitDevice(req, res) {
    const helper = new MobileControlerHelper() 
    const user = await helper.getUserByTokenOrSendStatus(req.headers, res)
    const devName = inputValidator.getValidParam(req.body.name, inputValidator.isValidDeviceName)

    if(!user) {
      // Do nothing
    }
    else if(!devName) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      await devMgr.preInitDevice(user, req.body.name)
      .then(deviceToken => {
        if(deviceToken === false) controlerResponses.sendDeviceNameAlreadyExistsError(res)
        else controlerResponses.sendStatusCreated(res, {deviceAccessToken: deviceToken})
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }

  async deleteDevice(req, res) {
    const helper = new MobileControlerHelper() 
    const user = await helper.getUserByTokenOrSendStatus(req.headers, res)
    const deviceId = inputValidator.getValidParam(req.body.deviceId, inputValidator.isValidDeviceId)

    if(!user) {
      // Do nothing
    }
    else if(!deviceId) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      devMgr.removeDevice(user, deviceId)
      .then(success => {
        if(!success) controlerResponses.sendDeviceNotFoundError(res)
        else controlerResponses.sendStatusOk(res)
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }
}

const mobileControler = new MobileControler
module.exports = mobileControler
