/*
 * @file		controlers-responeses.js
 * @brief		Responses for the controllers
 * @author	Dominik Kala
 * @date		2021-05-04
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

class ControlerResponsesCore {
  sendOkStatus(res, httpCode, data) {
    res.status(httpCode)
    if(data === undefined) res.send()
    else res.json(data)
  }
  
  sendErrorStatus(res, httpCode, message) {
    res.status(httpCode)
    res.json({msg: message})
  }
}

class ControlerResponses {
  constructor() {
    this.core = new ControlerResponsesCore()
  }

  sendStatusOk(res, data) {
    this.core.sendOkStatus(res, 200, data)
  }

  sendStatusCreated(res, data) {
    this.core.sendOkStatus(res, 201, data)
  }

  sendMissingHeaderError(res) {
    this.core.sendErrorStatus(res, 401, "Missing header or invalid format")
  }

  sendWronAuthenticationDataError(res) {
    this.core.sendErrorStatus(res, 401, "Wrong authentication data")
  }

  sendInvalidTokenError(res) {
    this.core.sendErrorStatus(res, 403, "Invalid token")
  }

  sendDeviceNotFoundError(res) {
    this.core.sendErrorStatus(res, 404, "Could not find a device in an acceptable state")
  }

  sendMissingDataError(res) {
    this.core.sendErrorStatus(res, 406, "Missing data or invalid format")
  }

  sendLoginAlreadyUsedError(res) {
    this.core.sendErrorStatus(res, 409, "Login already used")
  }

  sendDeviceNameAlreadyExistsError(res) {
    this.core.sendErrorStatus(res, 409, "Device name already exists")
  }

  sendInternalServerError(res) {
    this.core.sendErrorStatus(res, 500, "Internal server error")
  }

  sendNotImplementedError(res) {
    this.core.sendErrorStatus(res, 501, "Not implemented")
  }
}

const controlerResponses = new ControlerResponses()
module.exports = controlerResponses
