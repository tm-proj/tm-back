/*
 * @file		device-controler.js
 * @brief		Controler for the device routes 
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict';

const deviceTokenFactory = require('../objects/device-token-factory')
const inputValidator = require('../validators/input-validator')
const devMgr = require('../managers/device-mgr')
const measureMgr = require('../managers/measures-mgr')
const controlerResponses = require('./controler-responeses')
const measureFactory = require('../objects/measure')


/**
 * Device controler helper
 */
class DeviceControlerHelper {
  /**
   * 
   * @param {*} headers 
   * @param {*} res 
   * @returns 
   */
  getDeviceTokenOrSendStatus(headers, res) {
    const deviceTokenStr = inputValidator.getValidParam(headers['device-access-token'], inputValidator.isValidDeviceToken)
    if(!deviceTokenStr) {
      controlerResponses.sendMissingHeaderError(res)
    }
    else {
      const deviceToken = deviceTokenFactory.fromDeviceToken(deviceTokenStr)
      if(!deviceToken.isValid()) controlerResponses.sendInvalidTokenError(res)
      else return deviceToken
    }
    return false
  }
}

/**
 * Controler for the device routes 
 */
class DeviceControler {
  async initDevice(req, res) {
    const helper = new DeviceControlerHelper()
    const deviceToken = helper.getDeviceTokenOrSendStatus(req.headers, res)
    const mac = inputValidator.getValidParam(req.body.mac, inputValidator.isValidMac)

    if(!deviceToken) {
      // Do nothing
    }
    else if(!mac) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      devMgr.initDevice(deviceToken, mac)
      .then(success => {
        if(!success) controlerResponses.sendDeviceNotFoundError(res)
        else controlerResponses.sendStatusCreated(res)
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }

  async saveMeasure(req, res) {
    const helper = new DeviceControlerHelper()
    const deviceToken = helper.getDeviceTokenOrSendStatus(req.headers, res)
    const temperature = inputValidator.getValidParam(req.body.temperature, inputValidator.isValidFloatNumber)
    const humidity = inputValidator.getValidParam(req.body.humidity, inputValidator.isValidFloatNumber)
    const pm2_5 = inputValidator.getValidParam(req.body.pm2_5, inputValidator.isValidFloatNumber)
    const pm10 = inputValidator.getValidParam(req.body.pm10, inputValidator.isValidFloatNumber)
    
    if(!deviceToken) {
      // Do nothing
    }
    else if(!temperature || !humidity || !pm2_5 || !pm10) {
      controlerResponses.sendMissingDataError(res)
    }
    else {
      const measure = measureFactory.fromRawData(temperature, humidity, pm2_5, pm10)
      measureMgr.saveMeasure(deviceToken, measure)
      .then(success => {
        if(!success) controlerResponses.sendDeviceNotFoundError(res)
        else controlerResponses.sendStatusCreated(res)
      })
      .catch(err => {
        console.log(err)
        controlerResponses.sendInternalServerError(res)
      })
    }
  }
}

const deviceControler = new DeviceControler
module.exports = deviceControler
