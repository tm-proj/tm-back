/*
 * @file		input-validator.js
 * @brief		Input validator
 * @author	Dominik Kala
 * @date		2021-04-18
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

'use strict'

const regularExpressions = {
  login: /^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/,   // https://stackoverflow.com/a/12019115
  password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,             // https://stackoverflow.com/a/21456918
  jwt: /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/, // https://www.regextester.com/105777
  deviceToken: /^[A-Za-z0-9]{16}$/,
  mac: /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/,               // https://stackoverflow.com/a/4260512
  deviceId: /^\d{1,10}$/,
  deviceName: /^[a-zA-ZąĄćĆęĘłŁńŃóÓśŚźŹżŻ 0-9\-_]{5,200}$/,
  float: /^[+-]?([0-9]*[.])?[0-9]+$/,
  int: /^[+-]?[0-9]+$/
} 

/**
 * Input validator
 */
class InputValidator {
  /**
   * Check if provided login is valid
   * @param {string} login - user login
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidLogin(login) {
    return regularExpressions.login.test(login)
  }

  /**
   * Check if provided user password is valid
   * @param {string} pass - password
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidPassword(pass) {
    return regularExpressions.password.test(pass)
  }

  /**
   * Check if provided JWT is valid
   * @param {string} JWT - Json Web Token
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidJwt(jwt) {
    return regularExpressions.jwt.test(jwt)
  }

  /**
   * Check if provided device token is valid
   * @param {string} deviceToken - Device token
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidDeviceToken(deviceToken) {
    return regularExpressions.deviceToken.test(deviceToken)
  }

  /**
   * Check if provided MAC is valid
   * @param {string} mac - MAC address
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidMac(mac) {
    return regularExpressions.mac.test(mac)
  }

  /**
   * Check if provided device id is valid
   * @param {number} deviceId - Device id
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidDeviceId(deviceId) {
    return regularExpressions.deviceId.test(deviceId)
  }

  /**
   * Check if provided device name is valid
   * @param {string} deviceName - Device name
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidDeviceName(deviceName) {
    return regularExpressions.deviceName.test(deviceName)
  }

  /**
   * Check if provided float number is valid
   * @param {string|number} floatNum - float number
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidFloatNumber(floatNum) {
    return regularExpressions.float.test(floatNum)
  }

  /**
   * Check if provided int number is valid
   * @param {string|number} intNum - int number
   * @returns {boolean} true if is valid, false otherwise
   */
  isValidIntNumber(intNum) {
    return regularExpressions.int.test(intNum)
  }

  /**
   * Get valid parameter 
   * @param {string | number} param - Parameter to validate
   * @param {*} validator - Validator to check if parameter is valid
   * @returns {string | number | false} Returns passed parameter if valid, false otherwise
   */
  getValidParam(param, validator) {
    if(param === undefined || param === null || !validator(param)) return false
    return param
  }

  /**
   * Get valid array of parameters
   * @param {string[] | number[]} arr - Array to validate
   * @param {*} validator - Validator to check if parameter is valid
   * @returns {string[] | number[] | false} Returns array of valid parameters, false otherwise
   */
  getValidArray(arr, validator) {
    if(arr === undefined || arr === null) return false
    if(arr instanceof Array) {
      let result = []
      for(const elem in arr) {
        if(validator(arr[elem])) result.push(arr[elem])
      }
      if(result.length === 0) return false
      return result
    }
    return false
  }
}

const inputValidator = new InputValidator()
module.exports = inputValidator
