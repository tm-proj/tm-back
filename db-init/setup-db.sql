/*
 * @file		setup-db.sql
 * @brief		Script for initializing database
 * @author	Dominik Kala
 * @date		2021-3-25
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

USE tm_db;

-- Grant previlages for Api User
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'api_db_user'@'%';
GRANT SELECT, INSERT          ON `tm_user`            TO 'api_db_user'@'%';
GRANT SELECT                  ON `tm_dev_conf_status` TO 'api_db_user'@'%';
GRANT SELECT, INSERT, UPDATE  ON `tm_device`          TO 'api_db_user'@'%';
GRANT SELECT, INSERT          ON `tm_measure`         TO 'api_db_user'@'%';

-- Device configuration status
INSERT INTO `tm_dev_conf_status` (`id_dev_conf_status`, `name`) VALUES (1, 'Preinitialized');
INSERT INTO `tm_dev_conf_status` (`id_dev_conf_status`, `name`) VALUES (2, 'Initialized');
INSERT INTO `tm_dev_conf_status` (`id_dev_conf_status`, `name`) VALUES (3, 'Removed');

SELECT "DB set up done";
