#!/bin/bash
#
# @file	 Init-db.sh
# @brief  Script to initialize DB
# @author Dominik Kala
# @date   2021-3-26
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

CONT="tm-mysql-app"

# Based on https://unix.stackexchange.com/a/505342
helpFunction()
{
   echo ""
   echo "Usage: $0 -u dbUserName -p dbUserPassword"
   echo -e "\t-u DB user name"
   echo -e "\t-p DB user password"
   exit 1 # Exit script after printing help
}

while getopts "u:p:" opt
do
   case "$opt" in
      u ) DB_USER="$OPTARG" ;;
      p ) DB_PASS="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$DB_USER" ] || [ -z "$DB_PASS" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi
# End of stack code...

echo "Copying scripts..."
docker cp create-db.sql $CONT:/tmp/create-db.sql
docker cp setup-db.sql $CONT:/tmp/setup-db.sql
echo "Ok"

echo "Creating DB structure..."
docker exec $CONT mysql -u $DB_USER -p$DB_PASS tm_db -e "source /tmp/create-db.sql"
echo "Ok"

echo "Setting up DB..."
docker exec $CONT mysql -u $DB_USER -p$DB_PASS tm_db -e "source /tmp/setup-db.sql"
echo "Ok"

echo "Cleaning up..."
docker exec $CONT rm /tmp/create-db.sql
docker exec $CONT rm /tmp/setup-db.sql
echo "Ok"

echo "Done."
