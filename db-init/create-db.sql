/*
 * @file		create-db.sql
 * @brief		Script for creating database structure 
 * @author	Dominik Kala
 * @date		2021-3-25
 *
 * @note
 * Weahter station project
 * Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
 * Copyright (c) 2021 by Silesian University of Technology
 */

CREATE DATABASE IF NOT EXISTS tm_db;

USE tm_db;

DROP TABLE IF EXISTS `tm_measure`;
DROP TABLE IF EXISTS `tm_device`;
DROP TABLE IF EXISTS `tm_dev_conf_status`;
DROP TABLE IF EXISTS `tm_user`;

-- ************************************** `tm_user`

CREATE TABLE IF NOT EXISTS `tm_user`
(
 `id_user`  integer NOT NULL AUTO_INCREMENT ,
 `login`    varchar(200) NOT NULL ,
 `password` varchar(200) NOT NULL ,

PRIMARY KEY (`id_user`)
) CHARACTER SET=utf8;

-- ************************************** `tm_dev_conf_status`

CREATE TABLE IF NOT EXISTS `tm_dev_conf_status`
(
 `id_dev_conf_status` smallint NOT NULL ,
 `name`               varchar(50) NOT NULL ,

PRIMARY KEY (`id_dev_conf_status`)
) CHARACTER SET=utf8;

-- ************************************** `tm_device`

CREATE TABLE IF NOT EXISTS `tm_device`
(
 `id_device`          int NOT NULL AUTO_INCREMENT ,
 `name`               varchar(200) NOT NULL ,
 `mac`                varchar(20) DEFAULT NULL,
 `token`              varchar(16) DEFAULT NULL,
 `id_user`            integer NOT NULL ,
 `id_dev_conf_status` smallint NOT NULL ,

PRIMARY KEY (`id_device`),
KEY `fk_id_dev_conf_status` (`id_dev_conf_status`),
CONSTRAINT `FK_41` FOREIGN KEY `fk_id_dev_conf_status` (`id_dev_conf_status`) REFERENCES `tm_dev_conf_status` (`id_dev_conf_status`),
KEY `fk_id_user` (`id_user`),
CONSTRAINT `FK_38` FOREIGN KEY `fk_id_user` (`id_user`) REFERENCES `tm_user` (`id_user`)
) CHARACTER SET=utf8;

-- ************************************** `tm_measure`

CREATE TABLE IF NOT EXISTS `tm_measure`
(
 `id_measure`            int unsigned NOT NULL AUTO_INCREMENT ,
 `measure_utc_timestamp` timestamp NOT NULL ,
 `temp`                  float NOT NULL ,
 `humidity`              float NOT NULL ,
 `pm2_5`                 float NOT NULL ,
 `pm10`                  float NOT NULL ,
 `id_device`             int NOT NULL ,

PRIMARY KEY (`id_measure`),
KEY `fk_id_device` (`id_device`),
CONSTRAINT `FK_44` FOREIGN KEY `fk_id_device` (`id_device`) REFERENCES `tm_device` (`id_device`)
) CHARACTER SET=utf8;

SELECT "DB structure created";
