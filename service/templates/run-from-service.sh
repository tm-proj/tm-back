#!/bin/bash
#
# @file		run-from-service.sh
# @brief	Template for script to run app from service
# @author	Dominik Kala
# @date		2021-5-6
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

cd :rootDir
docker-compose up