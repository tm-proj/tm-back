#!/bin/bash
#
# @file		setup-service.sh
# @brief  Setup service
# @author	Dominik Kala
# @date		2021-05-05
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

serviceName="tm-back"
serviceDir=$(pwd)
serviceFile="${serviceName}.service"
runFromServiceFile="run-from-service.sh"
cd ..
rootDir=$(pwd)
cd $serviceDir
mkdir logs
logsDir="${serviceDir}/logs"

echo "Clean up previous version..."
systemctl stop $serviceName
rm rm-back.service
rm run-from-service.sh

echo "Setup scripts..."
cp "templates/${serviceFile}" $serviceFile
cp "templates/${runFromServiceFile}" $runFromServiceFile
chmod +x $runFromServiceFile
mkdir logs

echo "Configure scripts..."
sed -i 's,:serviceDir,'"$serviceDir"',g' $serviceFile
sed -i 's,:runScript,'"$runFromServiceFile"',g' $serviceFile
sed -i 's,:logsDir,'"$logsDir"',g' $serviceFile
sed -i 's,:logsDir,'"$logsDir"',g' $serviceFile
sed -i 's,:rootDir,'"$rootDir"',g' $runFromServiceFile

echo "Add service..."
ln -sf "${serviceDir}/${serviceFile}" "/etc/systemd/system/${serviceFile}"

echo "Enable service..."
systemctl enable $serviceName
echo "Start service..."
systemctl start $serviceName
systemctl status tm-back | head -n 3

echo "Done"