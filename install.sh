#!/bin/bash
#
# @file		install.sh
# @brief		type_brief_here
# @author	Dominik Kala
# @date		2021-5-10
#
# @note
# Weahter station project
# Created by Grzegorz Mrozek, Lukasz Plech and Dominik Kala
# Copyright (c) 2021 by Silesian University of Technology
#

OPT_ARM=false
COMPOSE_FILE="docker-compose.yml"
SETUP_DB_FILE="setup-db.sql"

BUILD_SCRIPT="build.sh"
INIT_SERVICE_SCRIPT="init-service.sh"
DB_INIT_SCRIPT="init-db.sh"

SERVICE_DIR="service"
DB_INIT_DIR="db-init"


# Based on https://unix.stackexchange.com/a/505342
helpFunction()
{
   echo ""
   echo "Usage: $0 -r rootPassword -u dbUserName -p dbUserPassword -a"
   echo -e "\t-r root password"
   echo -e "\t-u DB user name"
   echo -e "\t-p DB user password"
   echo -e "\t-a Pass this option to install for ARM (optional)"
   exit 1 # Exit script after printing help
}

while getopts "r:u:p:a" opt
do
   case "$opt" in
      r ) DB_ROOT_PASS="$OPTARG" ;;
      u ) DB_USER="$OPTARG" ;;
      p ) DB_PASS="$OPTARG" ;;
      a ) OPT_ARM=true ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$DB_ROOT_PASS" ] || [ -z "$DB_USER" ] || [ -z "$DB_PASS" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi
# End of stack code...

echo "Configuring..."

if $OPT_ARM;
then
  echo "Configuration for ARM..."
  sed -E 's/^[\# ]*(image:[ a-zA-Z0-9.\-\/:]+\#noArm)/    # \1/g' -i $COMPOSE_FILE
  sed -E 's/^[\# ]*(image:[ a-zA-Z0-9:\.\/\-]+\#arm)/    \1/g' -i $COMPOSE_FILE
else
  echo "Basic configuration..."
  sed -E 's/^[\# ]*(image:[ a-zA-Z0-9.\-\/:]+\#noArm)/    \1/g' -i $COMPOSE_FILE
  sed -E 's/^[\# ]*(image:[ a-zA-Z0-9:\.\/\-]+\#arm)/    # \1/g' -i $COMPOSE_FILE
fi
sed -E 's,(MYSQL_ROOT_PASSWORD=).*,\1'"$DB_ROOT_PASS"',g' -i $COMPOSE_FILE
sed -E 's,(MYSQL_USER=).*,\1'"$DB_USER"',g' -i $COMPOSE_FILE
sed -E 's,(MYSQL_USER_PASS=).*,\1'"$DB_PASS"',g' -i $COMPOSE_FILE
sed -E 's,(MYSQL_PASSWORD=).*,\1'"$DB_PASS"',g' -i $COMPOSE_FILE
cd $DB_INIT_DIR
sed -E "s/'[a-zA-Z0-9_]+'@'%'/'${DB_USER}'@'%'/g" -i $SETUP_DB_FILE
cd ..

echo "Cleaning up..."
docker stop tm-back-app || true && docker rm tm-back-app || true
docker stop tm-phpmyadmin-app || true && docker rm tm-phpmyadmin-app || true
docker stop tm-mysql-app || true && docker rm tm-mysql-app || true

echo "Building..."
sh $BUILD_SCRIPT

echo "Creating service..."
cd $SERVICE_DIR
sh $INIT_SERVICE_SCRIPT
cd ..

echo "Sleeping for 60s before trying to initialize DB..."
sleep 60
echo "Initializing DB..."
cd $DB_INIT_DIR
sh $DB_INIT_SCRIPT -u root -p $DB_ROOT_PASS
cd ..

echo "Done :)."
